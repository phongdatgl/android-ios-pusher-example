<?php 
// Server file
error_reporting(E_ALL | E_STRICT);
include 'APNSBase.php';
include 'APNotification.php';
include 'Pusher.php';

class PushNotifications_Model extends CI_Model{


	public function sendBadgeNotification(){
		$msg_payload = array (
			'mtitle' => 'Test push notification title',
			'mdesc' => 'Test push notification body',
		);

		$gcm_key = "AIzaSyB7ogGWd6WhWFf-2Q0XHF2xdbfYRuwlneI";
		//$gcm_key = "APA91bGTNoBtcY0MyACmQRTs3R4IvDU-5SQOpg5cKAval1_NungMUpY1B4Z-eR2f4gt-ZhcYfQDTQiZa50dXkQS1jc1VAqutnh0IldUid6NIbyOZUnjJ8fo";

		$query = $this->db->get_where('tbl_mobile_token', array('devtype' => 'iPhone'));
		$result = $query->result_array();
		if(count($result)>0){
			for($i = 0; $i < count($result); $i++) {

			          $this->db->where('devidtoken', $result[$i]['devidtoken']);
			          $this->db->update('tbl_mobile_token', array('badgeNumber' => $result[$i]['badgeNumber'] + 1));
			            
				/*$notification = new APNotification('development');
				$notification->setDeviceToken($result[$i]['devidtoken']);
				$notification->setMessage("Test Push");
				$notification->setBadge(((int)$result[$i]['badgeNumber']) + 1);
				//$notification->setPrivateKey('Certificates/devPushNotifications.pem');
				$notification->setPrivateKey('Certificates/RSTest.pem');
				$notification->setPrivateKeyPassphrase('Aquarius0821');
				$notification->send();*/
				
				// Put your device token here (without spaces):
				$deviceToken = str_replace(" ", "", $result[$i]['devidtoken']);
				//$deviceToken = '3ec009129cafd2a4286eb5e6ef97f1925ccf15b6889cae7796ec4289469f9af9';
				$passphrase = 'Runsystem!@#4';
				//$passphrase = 'Aquarius0821';

				$message = 'DEV MESS 123';
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates/RSTest.pem');
				stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

				$fp = stream_socket_client(
					'ssl://gateway.sandbox.push.apple.com:2195', $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

				$body['aps'] = array(
					'alert' => $message,
					'badge' => 1,
					'sound' => 'default'
					);
				$payload = json_encode($body);

				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

				$result = fwrite($fp, $msg, strlen($msg));
				fclose($fp);

			}
		}

		$query = $this->db->get_where('tbl_mobile_token', array('devtype' => 'Android'));
		$result = $query->result_array();
		if(count($result)>0){
			for($i = 0; $i < count($result); $i++) {
				$pusher = new AndroidPusher\Pusher($gcm_key);
				$deviceToken = $result[$i]['devidtoken'];
				$pusher->notify($deviceToken, "Badge");
			}
		}
	}
}
?>